import React from "react";

export default function Model({ glass }) {
  const { url, desc, name, price } = glass;
  return (
    <div className="card mx-auto border-primary">
      <img src="./glassesImage/model.jpg" className="card-img-top" alt="true" />
      {url.length > 0 && (
        <img
          src={url}
          className="position-absolute opacity-75 mx-auto"
          style={{
            left: 4,
            right: 0,
            top: 150,
            width: "290px",
          }}
          alt="glass"
        />
      )}
      <div className="card-body text-start">
        <h5 className="card-title">Tên: {name}</h5>
        <p className="card-text">Mô tả sản phẩm: {desc}</p>
        <h3 className="card-text font-weight-bold">Giá: {price}$</h3>
      </div>
    </div>
  );
}
