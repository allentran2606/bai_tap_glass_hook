import React, { memo } from "react";

function GlassList({ dataGlass, setGlass }) {
  return (
    <div className="row g-3 bg-dark p-4">
      {dataGlass.map((item, index) => {
        return (
          <div key={index} className="col-4 my-2">
            <img
              style={{ cursor: "pointer" }}
              className="img-fluid"
              src={item.url}
              onClick={() => setGlass(item)}
              alt=""
            />
          </div>
        );
      })}
    </div>
  );
}

export default memo(GlassList);
